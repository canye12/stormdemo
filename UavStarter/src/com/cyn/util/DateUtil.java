package com.cyn.util;

import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static Date setTimeHhMmSs(int hh, int mm, int ss, int ms) {
		Calendar now = Calendar.getInstance();
		now.set(Calendar.HOUR_OF_DAY, hh);
		now.set(Calendar.MINUTE, mm);
		now.set(Calendar.SECOND, ss);
		now.set(Calendar.MILLISECOND, ms);
		return now.getTime();
	}

	public static Date parseDate(String locTime) {
		Date date = null;
		double locTimeDouble = Double.parseDouble(locTime);
		locTimeDouble = locTimeDouble / 1000;
		int locTimeInt = (int) locTimeDouble;
		int ms = (int) ((locTimeDouble - locTimeInt) * 1000);
		int ss = locTimeInt % 60;
		int mm = (locTimeInt - ss) / 60 % 60;
		int hh = (locTimeInt - ss - mm * 60) / 3600;
		date = setTimeHhMmSs(hh, mm, ss, ms);
		return date;
	}
}
