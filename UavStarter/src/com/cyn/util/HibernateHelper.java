package com.cyn.util;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.boot.registry.internal.StandardServiceRegistryImpl;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateHelper {

	private SessionFactory sessionFactory;
	public ThreadLocal<Session> session = new ThreadLocal<Session>();

	public HibernateHelper(String file) {
		this.LoadConfig(file);
	}

	// 加载配置
	public void LoadConfig(String file) {
		if (file == null || file.equals("")) {
			return;
		}

		try {
			Configuration config = new Configuration().configure(file);
			StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder()
					.applySettings(config.getProperties());
			StandardServiceRegistryImpl registry = (StandardServiceRegistryImpl) builder.build();
			sessionFactory = config.buildSessionFactory(registry);
		} catch (Throwable e) {
			// throw new ExceptionInInitializerError(e);
			e.printStackTrace();
		}
	}

	// 当前会话
	public Session getCurrentSession() throws HibernateException {
		if (sessionFactory == null)
			return null;

		Session s = (Session) session.get();
		// Open a new Session,if this Thread has none yet
		if (s == null || !s.isOpen()) {
			s = sessionFactory.openSession();
			session.set(s);
		}
		return s;
	}

	// 关闭会话
	public void closeSession() throws HibernateException {
		Session s = (Session) session.get();
		session.set(null);
		if (s != null)
			s.close();
	}
}
