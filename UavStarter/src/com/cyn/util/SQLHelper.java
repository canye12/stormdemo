package com.cyn.util;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.type.Type;

public class SQLHelper {

	private HibernateHelper hibernateHelper;

	public SQLHelper(String file) {
		hibernateHelper = new HibernateHelper(file);
	}

	// loadConfig
	public void LoadConfig(String file) {
		hibernateHelper.LoadConfig(file);
	}

	public Session getSession() {
		return hibernateHelper.getCurrentSession();
	}

	// 查询
	public <T> List<T> queryAll(String tableName) {
		Query query = getSession().createQuery("from " + tableName);
		return query.list();
	}

	// 查询符合条件 的记录
	public <T> List<T> query(String tableName, String filter, Object[] paramValues, Type[] paramTypes, String order) {
		String sql = "from " + tableName;
		if (filter != null && !filter.equals("")) {
			sql += " where " + filter;
		}
		if (order != null && !order.equals("")) {
			sql += " order by " + order;
		}

		try {
			Query query = getSession().createQuery(sql);
			if (paramValues != null && paramValues.length > 0) {
				query.setParameters(paramValues, paramTypes);
			}

			return query.list();
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	// 查询符合条件 的记录
	public <T> List<T> query(String tableName, String filter, Map<String, Object> paramItems, String order) {
		String sql = "from " + tableName;
		if (filter != null && !filter.equals("")) {
			sql += " where " + filter;
		}
		if (order != null && !order.equals("")) {
			sql += " order by " + order;
		}

		try {
			Query query = getSession().createQuery(sql);

			if (paramItems != null && paramItems.size() > 0) {
				Object[] pairs = paramItems.entrySet().toArray();
				for (int i = 0; i < pairs.length; i++) {
					Entry item = (Entry) pairs[i];
					if (item.getValue() instanceof Date) {
						query.setTimestamp(item.getKey().toString(), (Date) item.getValue());
					} else {
						query.setParameter(item.getKey().toString(), item.getValue());
					}
				}
			}

			return query.list();
		} catch (Exception e) {
			e.printStackTrace();

			return null;
		}
	}

	// 查询符合条件 的记录
	public <T> List<T> query(String tableName, String filter, Map<String, Object> paramItems) {
		return query(tableName, filter, paramItems, "");
	}

	// 查询符合条件 的记录
	public <T> List<T> query(String tableName, String filter, String order) {
		return query(tableName, filter, null, order);
	}

	// 查询符合条件 的记录
	public <T> List<T> query(String tableName, String filter) {
		return query(tableName, filter, null, "");
	}

	// 查询符合条件的记录
	public <T> List<T> queryByField(String tableName, String fieldName, String value) {
		Query query = getSession().createQuery("from " + tableName + " where " + fieldName + " = '" + value + "'");
		return query.list();
	}
}
