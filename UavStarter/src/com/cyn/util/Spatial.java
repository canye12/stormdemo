package com.cyn.util;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;
import com.vividsolutions.jts.io.WKTWriter;

public class Spatial {
	
	//输出wkt格式字符串
	public static String writeWKT(Geometry geom) {
		WKTWriter writer = new WKTWriter();
		return writer.write(geom);
	}
	
	//wkt格式转换成空间类型
	public static Geometry readWKT(String wktGeom) throws Exception{
		WKTReader reader=new WKTReader();
		return reader.read(wktGeom);
	}
}
