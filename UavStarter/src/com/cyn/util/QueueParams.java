package com.cyn.util;

public class QueueParams implements java.io.Serializable {
	String server;
	int port;
	String exchangeName;
	String queueName;
	String routingKey;
	String virtualHost;
	String userName;
	String password;

	public QueueParams(String server, int port, String exchangeName, String queueName, String routingKey,
			String virtualHost, String userName, String password) {

		this.server = server;
		this.port = port;
		this.exchangeName = exchangeName;
		this.queueName = queueName;
		this.routingKey = routingKey;
		this.virtualHost = virtualHost;
		this.userName = userName;
		this.password = password;

	}

	public void setServer(String value) {
		this.server = value;
	}

	public String getServer() {
		return this.server;
	}

	public void setPort(int value) {
		this.port = value;
	}

	public int getPort() {
		return this.port;
	}

	public void setExchangeName(String value) {
		this.exchangeName = value;
	}

	public String getExchangeName() {
		return this.exchangeName;
	}

	public void setQueueName(String value) {
		this.queueName = value;
	}

	public String getQueueName() {
		return this.queueName;
	}

	public void setRoutingKey(String value) {
		this.routingKey = value;
	}

	public String getRoutingKey() {
		return this.routingKey;
	}

	public void setVirtualHost(String value) {
		this.virtualHost = value;
	}

	public String getVirtualHost() {
		return this.virtualHost;
	}

	public void setUserName(String value) {
		this.userName = value;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setPassword(String value) {
		this.password = value;
	}

	public String getPassword() {
		return this.password;
	}
}
