package com.cyn.util;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;

public class MGDBHelper {
	private static Mongo mongo = null;
	private static DB db = null;

	static {
		mongo = new Mongo();
		db = mongo.getDB("ucloud");
	}

	// 获取集合
	public static DBCollection getCollection(String name) {
		return db.getCollection(name);
	}

	// 查询所有数据
	public static List<DBObject> queryAll(String collection) {
		List<DBObject> models = new ArrayList<DBObject>();
		DBCursor cursor = getCollection(collection).find();
		while (cursor.hasNext()) {
			models.add(cursor.next());
		}
		return models;
	}

	// 根据字段过滤
	public static List<DBObject> queryBy(String collection, String field, String value) {
		List<DBObject> models = new ArrayList<DBObject>();
		DBCursor cursor = getCollection(collection).find(new BasicDBObject(field, value));
		while (cursor.hasNext()) {
			models.add(cursor.next());
		}
		return models;
	}

	// 查询符合条件的单条记录
	public static DBObject queryOne(String collection, String field, String value) {
		return queryBy(collection, field, value).get(0);
	}
	
	//查询总记录数
	public static long queryCount(String collection) {
		return getCollection(collection).count();
	}

	// 添加
	public static void insert(String collection, DBObject model) {
		getCollection(collection).insert(model);
	}
}
