package com.cyn.util;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;
import com.rabbitmq.client.ShutdownSignalException;

public class RabbitHelper {

	private static Connection conn = null;
	private static Channel channel = null;

	// 创建通道
	private static Channel createChannel(QueueParams params) throws IOException, TimeoutException {

		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(params.getServer());
		factory.setPort(params.getPort());

		factory.setVirtualHost(params.getVirtualHost());
		factory.setUsername(params.getUserName());
		factory.setPassword(params.getPassword());

		conn = factory.newConnection();
		channel = conn.createChannel();
		channel.queueDeclare(params.queueName, false, true, true, null);
		//channel.exchangeDeclare(params.getExchangeName(), "direct", true);
		channel.queueBind(params.getQueueName(), params.getExchangeName(), params.getRoutingKey());

		return channel;
	}

	// 创建一个消费者
	public static QueueingConsumer createConsumer(QueueParams params) throws IOException, TimeoutException,
			ShutdownSignalException, ConsumerCancelledException, InterruptedException {
		Channel channel = createChannel(params);
		QueueingConsumer consumer = new QueueingConsumer(channel);
		channel.basicConsume(params.getQueueName(), true, consumer);

		return consumer;
	}

	// 关闭
	public static void close() throws IOException, TimeoutException {
		if (channel != null) {
			channel.close();
			channel = null;
		}

		if (conn != null) {
			conn.close();
			conn = null;
		}
	}

	// 运行测试
	public static void runTest() throws ShutdownSignalException, ConsumerCancelledException, InterruptedException,
			IOException, TimeoutException {
		QueueParams params = new QueueParams("121.69.39.114", 5672, "amq.direct", "test", "ucloud", "ucloud", "drone",
				"123");
		QueueingConsumer consumer = createConsumer(params);
		String msg = "";

		while (true) {
			Delivery delivery = consumer.nextDelivery();
			msg = new String(delivery.getBody(), "utf-8");
			System.out.println(msg);
		}
	}
}
