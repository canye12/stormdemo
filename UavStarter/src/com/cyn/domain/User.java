package com.cyn.domain;

public class User implements java.io.Serializable {

	String id;
	String name;

	User() {
	}

	private void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}
}
