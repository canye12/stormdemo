package com.cyn.domain;

import java.util.Date;

public class FlightPlan implements java.io.Serializable {
	private String id;
	private String bdId;
	private String name;
	private String areaId;
	private Date beginTime;
	private Date endTime;
	private double height;
	public int state;

	public void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id;
	}

	public void setBdId(String value) {
		this.bdId = value;
	}

	public String getBdId() {
		return this.bdId;
	}
	
	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}

	public void setAreaId(String value) {
		this.areaId = value;
	}

	public String getAreaId() {
		return this.areaId;
	}

	public void setBeginTime(Date value) {
		this.beginTime = value;
	}

	public Date getBeginTime() {
		return this.beginTime;
	}

	public void setEndTime(Date value) {
		this.endTime = value;
	}

	public Date getEndTime() {
		return this.endTime;
	}

	public void setHeight(double value) {
		this.height = value;
	}

	public double getHeight() {
		return this.height;
	}

	public void setState(int value) {
		this.state = value;
	}

	public int getState() {
		return this.state;
	}

}
