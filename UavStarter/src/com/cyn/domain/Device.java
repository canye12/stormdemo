package com.cyn.domain;

public class Device implements java.io.Serializable {
	private String id;
	private String bdId;
	private String flightId;
	private String valid;
	private String validFlag;
	
	Device(){
		
	}
	
	public void setId(String value){
		this.id=value;
	}
	
	public String getId(){
		return this.id;
	}
	
	public void setBdId(String value){
		this.bdId=value;
	}
	
	public String getBdId(){
		return this.bdId;
	}
	
	public void setFlightId(String value){
		this.flightId=value;
	}
	
	public String getFlightId(){
		return this.flightId;
	}
	
	public void setValid(String value){
		this.valid=value;
	}
	
	public String getValid(){
		return this.valid;
	}
	
	public void setValidFlag(String value){
		this.validFlag=value;
	}
	
	public String getValidFlag(){
		return this.validFlag;
	}
}
