package com.cyn.domain;

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public class FlightArea implements java.io.Serializable {
	private String id;
	private String name;
	private Point areacenter;
	private Polygon coord;

	public void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id;
	}

	public void setName(String value) {
		this.name = value;
	}

	public String getName() {
		return this.name;
	}

	public void setAreacenter(Point value) {
		this.areacenter = value;
	}

	public Point getAreacenter() {
		return this.areacenter;
	}

	public void setCoord(Polygon value) {
		this.coord = value;
	}

	public Polygon getCoord() {
		return this.coord;
	}
}
