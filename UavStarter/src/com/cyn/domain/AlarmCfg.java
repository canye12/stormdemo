package com.cyn.domain;

import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;

public class AlarmCfg implements java.io.Serializable {
	private String id;
	private String alarmType;
	private int alarmLevel;
	private double alarmHeight;
	private String remark;

	public void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id;
	}

	public void setAlarmType(String value) {
		this.alarmType = value;
	}

	public String getAlarmType() {
		return this.alarmType;
	}

	public void setAlarmLevel(int value) {
		this.alarmLevel = value;
	}

	public int getAlarmLevel() {
		return this.alarmLevel;
	}

	public void setAlarmHeight(double value) {
		this.alarmHeight = value;
	}

	public double getAlarmHeight() {
		return this.alarmHeight;
	}

	public void setRemark(String value) {
		this.remark = value;
	}

	public String getRemark() {
		return this.remark;
	}
}
