package com.cyn.domain;

import java.util.Date;

public class AlarmInfo implements java.io.Serializable {
	private String id;
	private String bdId;
	private String alarmType;
	private int alarmLevel;
	private double alarmHeight;
	private String planId;

	public void setId(String value) {
		this.id = value;
	}

	public String getId() {
		return this.id;
	}

	public void setBdId(String value) {
		this.bdId = value;
	}

	public String getBdId() {
		return this.bdId;
	}

	public void setAlarmType(String value) {
		this.alarmType = value;
	}

	public String getAlarmType() {
		return this.alarmType;
	}

	public void setAlarmLevel(int value) {
		this.alarmLevel = value;
	}

	public int getAlarmLevel() {
		return this.alarmLevel;
	}

	public void setAlarmHeight(double value) {
		this.alarmHeight = value;
	}

	public double getAlarmHeight() {
		return this.alarmHeight;
	}

	public void setPlanId(String value) {
		this.planId = value;
	}

	public String getPlanId() {
		return this.planId;
	}
}
