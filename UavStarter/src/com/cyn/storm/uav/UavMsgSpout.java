package com.cyn.storm.uav;

import java.util.Map;

import com.cyn.util.QueueParams;
import com.cyn.util.RabbitHelper;
import com.rabbitmq.client.QueueingConsumer;
import com.rabbitmq.client.QueueingConsumer.Delivery;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

public class UavMsgSpout extends BaseRichSpout {
	SpoutOutputCollector _collector;
	QueueParams params = new QueueParams("121.69.39.114", 5672, "amq.direct", "", "test", "test", "drone", "123");
	QueueingConsumer consumer = null;

	@Override
	public void nextTuple() {
		Delivery delivery;

		try {
			delivery = consumer.nextDelivery();
			String msg = new String(delivery.getBody(), "utf-8");
			System.out.println("接收到数据：" + msg);

			_collector.emit(new Values(msg));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
		_collector = collector;

		try {
			if (consumer == null) {
				consumer = RabbitHelper.createConsumer(params);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("msg"));
	}
}
