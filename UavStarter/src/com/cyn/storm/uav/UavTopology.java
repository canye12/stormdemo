package com.cyn.storm.uav;

import com.cyn.storm.uav.bolt.AlarmHeightBolt;
import com.cyn.storm.uav.bolt.ParseBolt;
import com.cyn.storm.uav.bolt.PlanBindBolt;
import com.cyn.storm.uav.bolt.UavAuthBolt;

import backtype.storm.topology.TopologyBuilder;

public class UavTopology {

	// 注册作业
	public void register(TopologyBuilder builder) {
		builder.setSpout("receiveMsg", new UavMsgSpout(), 1);

		// 解析
		builder.setBolt("parse", new ParseBolt(), 2).shuffleGrouping("receiveMsg");

		// 验证无人机是否有效
		builder.setBolt("uavAuth", new UavAuthBolt(),2).shuffleGrouping("parse");
		
		//计划绑定
		builder.setBolt("planbind", new PlanBindBolt(),2).shuffleGrouping("uavAuth");
		
		//高度告警处理
		builder.setBolt("alarm_height",new AlarmHeightBolt()).shuffleGrouping("planbind");
		
		//区域告警处理
		//builder.setBolt("alarm_area",new AlarmAreaBolt()).shuffleGrouping("alarm_height");
		
		// 数据持久化（存储）
//		builder.setBolt("persistent", new DataPersistentBolt()).shuffleGrouping("alarm_area");
	}
}
