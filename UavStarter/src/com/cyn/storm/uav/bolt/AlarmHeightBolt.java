package com.cyn.storm.uav.bolt;

import java.util.List;

import com.cyn.domain.AlarmCfg;
import com.cyn.domain.AlarmInfo;
import com.cyn.model.FlyInfo;
import com.cyn.util.SQLHelper;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class AlarmHeightBolt extends BaseBasicBolt {

	private static SQLHelper sqlHelper;

	private AlarmInfo createAlarmInfo(FlyInfo flyInfo) {
		AlarmInfo alarm = new AlarmInfo();
		alarm.setBdId(flyInfo.getBdId());
		alarm.setAlarmType("501");
		alarm.setAlarmLevel(0);
		alarm.setAlarmHeight(flyInfo.getAltitude());

		return alarm;
	}

	// 处理配置的高度告警
	private void handleAlarmCfg(FlyInfo flyInfo) {
		List<AlarmCfg> cfgModels = sqlHelper.query("AlarmCfg", "alarmType='501'", " alarmLevel asc");
		int size = cfgModels.size();
		if (size > 0) {
			AlarmCfg m = cfgModels.get(0);
			// 最高级别告警
			if (flyInfo.getAltitude() >= m.getAlarmHeight()) {
				AlarmInfo alarm = createAlarmInfo(flyInfo);
				alarm.setAlarmLevel(m.getAlarmLevel());

				flyInfo.addAlarm(alarm);
			} else {
				for (int i = size - 1; i > 0; i--) {
					if ((flyInfo.getAltitude() < cfgModels.get(i).getAlarmHeight()
							&& flyInfo.getAltitude() > cfgModels.get(i - 1).getAlarmHeight())
							|| flyInfo.getAltitude() == cfgModels.get(i - 1).getAlarmHeight()) {
						AlarmInfo alarm = createAlarmInfo(flyInfo);
						alarm.setAlarmLevel(cfgModels.get(i - 1).getAlarmLevel());

						flyInfo.addAlarm(alarm);
					}
				}
			}
		}
	}

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		if (sqlHelper == null) {
			sqlHelper = new SQLHelper("mysql.cfg.xml");
		}

		FlyInfo flyInfo = (FlyInfo) tuple.getValueByField("flyInfo");

		if (flyInfo.getPlan() != null) {// 判断计划高度告警
			if (flyInfo.getAltitude() > flyInfo.getPlan().getHeight()) {
				AlarmInfo alarm = createAlarmInfo(flyInfo);
				alarm.setPlanId(flyInfo.getPlan().getId());

				flyInfo.addAlarm(alarm);
			}
		} else {// 判断配置高度告警
			handleAlarmCfg(flyInfo);
		}

		if (flyInfo.getAlarms() != null && flyInfo.getAlarms().size() > 0) {
			AlarmInfo alarm = flyInfo.getAlarms().get(0);
			System.out.println(flyInfo.getBdId() + " 区域告警信息：告警高度：" + alarm.getAlarmHeight() + " 等级："
					+ String.valueOf(alarm.getAlarmLevel()) + " 计划："
					+ ((alarm.getPlanId() == null || alarm.getBdId().equals("")) ? "无" : "计划" + alarm.getPlanId()));
		}

		collector.emit(new Values(flyInfo));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("flyInfo"));
	}

}
