package com.cyn.storm.uav.bolt;

import com.cyn.util.MGDBHelper;
import com.mongodb.DBObject;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

public class DataPersistentBolt extends BaseBasicBolt {

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		DBObject uavInfo = (DBObject) tuple.getValueByField("flyInfo");
		
		MGDBHelper.insert("tdevicedata_uav", uavInfo);
		System.out.println("已存储数量：" + MGDBHelper.queryCount("tdevicedata_uav"));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("uavInfo"));
	}

}
