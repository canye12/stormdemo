package com.cyn.storm.uav.bolt;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cyn.domain.FlightPlan;
import com.cyn.model.FlyInfo;
import com.cyn.util.SQLHelper;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class PlanBindBolt extends BaseBasicBolt {

	private static SQLHelper sqlHelper;

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		if (sqlHelper == null) {
			sqlHelper = new SQLHelper("mysql.cfg.xml");
		}

		FlyInfo model = (FlyInfo) tuple.getValue(0);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("postime", model.getPosTime());
		List<FlightPlan> planModels = sqlHelper.query("FlightPlan",
				" ( beginTime<:postime and endTime>:postime ) or beginTime=:postime or endTime=:postime", params);
		if (planModels.size() > 0) {
			FlightPlan plan = planModels.get(0);
			model.setPlan(plan);

			System.out.println(model.getBdId() + "  已绑定计划     " + plan.getName());
		}

		collector.emit(new Values(model));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("flyInfo"));
	}

}
