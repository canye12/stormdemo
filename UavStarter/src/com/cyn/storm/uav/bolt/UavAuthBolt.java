package com.cyn.storm.uav.bolt;

import java.util.List;

import com.cyn.domain.Device;
import com.cyn.model.FlyInfo;
import com.cyn.util.SQLHelper;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class UavAuthBolt extends BaseBasicBolt {

	private static SQLHelper sqlHelper;

	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		if (sqlHelper == null) {
			sqlHelper = new SQLHelper("mysql.cfg.xml");
		}

		FlyInfo flyInfo = (FlyInfo) tuple.getValue(0);

		String bdId = flyInfo.getBdId();
		List<Device> models = sqlHelper.queryByField("Device", "bdId", bdId);
		if (models.size() == 0) {
			System.out.println("无人机不存在：" + bdId);
		} else {
			Device model = (Device) models.get(0);
			// 无效
			if (model.getValidFlag() == "0") {
				// System.out.println("无人机无效：" + bdId);
				return;
			}

			System.out.println("无人机验证通过：" + bdId);

			collector.emit(new Values(flyInfo));
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("flyInfo"));
	}

}
