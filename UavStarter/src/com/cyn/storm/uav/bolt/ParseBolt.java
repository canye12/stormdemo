package com.cyn.storm.uav.bolt;

import java.math.BigDecimal;
import java.util.Date;

import com.cyn.util.MGDBHelper;
import com.cyn.model.FlyInfo;
import com.cyn.util.DateUtil;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import backtype.storm.topology.BasicOutputCollector;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseBasicBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

public class ParseBolt extends BaseBasicBolt {
	
	@Override
	public void execute(Tuple tuple, BasicOutputCollector collector) {
		String msg = tuple.getString(0);
		FlyInfo flyInfo=null;
		
		try{
			flyInfo=FlyInfo.parse(msg);
		}
		catch(Exception e){
			e.printStackTrace();
			return;
		}
		
		String bdId = flyInfo.getBdId();
		if(bdId==null || bdId.equals("")){
			System.out.println("数据无效，设备号为空");
			return;
		}
		
		System.out.println("解析后数据：" + bdId+",时间："+flyInfo.getPosTime().toString());
		
		collector.emit(new Values(flyInfo));
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer ofd) {
		ofd.declare(new Fields("flyInfo"));
	}

}
