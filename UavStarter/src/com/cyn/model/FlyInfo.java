package com.cyn.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.cyn.domain.AlarmInfo;
import com.cyn.domain.FlightPlan;
import com.cyn.util.DateUtil;

public class FlyInfo implements java.io.Serializable {
	String bdId;
	double longitude;
	double latitude;
	double altitude;
	double gpsAltitude;
	double course;
	double pitchAngle;
	double rollAngle;
	double speed;
	Date posTime;

	FlightPlan plan;// 绑定的计划信息
	List<AlarmInfo> alarms;

	public void setBdId(String value) {
		this.bdId = value;
	}

	public String getBdId() {
		return this.bdId;
	}

	public void setLongitude(double value) {
		this.longitude = value;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public void setLatitude(double value) {
		this.latitude = value;
	}

	public double getLatitude() {
		return this.longitude;
	}

	public void setAltitude(double value) {
		this.altitude = value;
	}

	public double getAltitude() {
		return this.altitude;
	}

	public void setGpsAltitude(double value) {
		this.gpsAltitude = value;
	}

	public double getGpsAltitude() {
		return this.gpsAltitude;
	}

	public void setCourse(double value) {
		this.course = value;
	}

	public double getCourse() {
		return this.course;
	}

	public void setPitchAngle(double value) {
		this.pitchAngle = value;
	}

	public double getPitchAngle() {
		return this.pitchAngle;
	}

	public void setRollAngle(double value) {
		this.rollAngle = value;
	}

	public double getRollAngle() {
		return this.rollAngle;
	}

	public void setSpeed(double value) {
		this.speed = value;
	}

	public double getSpeed() {
		return this.speed;
	}

	public void setPosTime(Date value) {
		this.posTime = value;
	}

	public Date getPosTime() {
		return this.posTime;
	}

	public void setPlan(FlightPlan value) {
		this.plan = value;
	}

	public FlightPlan getPlan() {
		return this.plan;
	}

	public void setAlarms(List<AlarmInfo> value) {
		this.alarms = value;
	}

	public List<AlarmInfo> getAlarms() {
		return this.alarms;
	}
	
	//添加告警
	public void addAlarm(AlarmInfo value) {
		if (this.alarms == null) {
			this.alarms = new ArrayList<AlarmInfo>();
		}

		this.alarms.add(value);
	}

	// 解析字符串
	public static FlyInfo parse(String msg) throws Exception {
		if (!msg.startsWith("#FDS_TRACK") || !msg.endsWith("*")) {
			throw new Exception("数据格式有误");
		}

		msg = msg.substring(0, msg.length() - 1);
		String[] messageArr = msg.split(",");
		if (messageArr.length != 11) {
			throw new Exception("数据格式有误");
		}

		FlyInfo model = new FlyInfo();
		model.setBdId(messageArr[1].trim());

		String longitudeStr = messageArr[2].trim();
		String latitudeStr = messageArr[3].trim();
		String altitudeStr = messageArr[4].trim();// 离地高度
		String gpsAltitudeStr = messageArr[5].trim();// GPS海拔高度
		String courseStr = messageArr[6].trim();
		String pitchAngleStr = messageArr[7].trim();
		String rollAngleStr = messageArr[8].trim();
		String speedStr = messageArr[9].trim();
		String locTime = messageArr[10].trim();

		try {

			double latitude = (new BigDecimal(Double.parseDouble(latitudeStr) / 10000000))
					.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setLatitude(latitude);

			double longitude = (new BigDecimal(Double.parseDouble(longitudeStr) / 10000000))
					.setScale(6, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setLongitude(longitude);

			double altitude = (new BigDecimal(Double.parseDouble(altitudeStr) / 1000))
					.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setAltitude(altitude);

			double gpsAltitude = (new BigDecimal(Double.parseDouble(gpsAltitudeStr) / 1000))
					.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setGpsAltitude(gpsAltitude);

			double course = (new BigDecimal(Double.parseDouble(courseStr) / 10)).setScale(3, BigDecimal.ROUND_HALF_UP)
					.doubleValue();
			model.setCourse(course);

			double pitchAngle = (new BigDecimal(Double.parseDouble(pitchAngleStr) / 10))
					.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setPitchAngle(pitchAngle);

			double rollAngle = (new BigDecimal(Double.parseDouble(rollAngleStr) / 10))
					.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
			model.setRollAngle(rollAngle);

			double speed = (new BigDecimal(Double.parseDouble(speedStr) / 100)).setScale(3, BigDecimal.ROUND_HALF_UP)
					.doubleValue();
			model.setSpeed(speed);

			Date posTime = DateUtil.parseDate(locTime);
			model.setPosTime(posTime);
		} catch (Exception e) {
			throw e;
		}

		return model;
	}
}
