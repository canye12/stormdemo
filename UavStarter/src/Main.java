import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cyn.domain.FlightArea;
import com.cyn.domain.FlightPlan;
import com.cyn.storm.uav.UavTopology;
import com.cyn.util.SQLHelper;
import com.cyn.util.Spatial;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.utils.Utils;

public class Main {

	public static void main(String[] args) throws Exception {
		// testMysql();
		// testPostsql();
		startUavTopology(args);
	}

	// 开启消息队列接收
	private static void startUavTopology(String[] args) throws Exception {
		TopologyBuilder builder = new TopologyBuilder();

		UavTopology uavTopology = new UavTopology();
		uavTopology.register(builder);

		Config conf = new Config();
		conf.setDebug(false);
		// conf.setMessageTimeoutSecs(1000);

		if (args != null && args.length > 0) {
			conf.setNumWorkers(1);
			StormSubmitter.submitTopologyWithProgressBar(args[0], conf, builder.createTopology());
		} else {
			LocalCluster cluster = new LocalCluster();
			StormTopology topology = builder.createTopology();
			cluster.submitTopology("test", conf, topology);
			Utils.sleep(3600000);
			cluster.killTopology("test");
			cluster.shutdown();
		}
	}

	private static void testMysql() throws Exception {
		SQLHelper sqlHelper = new SQLHelper("mysql.cfg.xml");

		// List<User> models = SQLHelper.query("User");
		// for (int i = 0; i < models.size(); i++) {
		// System.out.println(models.get(i).getName());
		// }
		// List<Device> models = SQLHelper.queryByField("Device", "bdId",
		// "E-9093");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("postime", dateFormat.parse("2016-01-11 18:48:00"));

		List<FlightPlan> models = sqlHelper.query("FlightPlan", "beginTime<:postime and endTime>:postime", params);
		if (models.size() > 0) {
			FlightPlan model = models.get(0);
			System.out.println(model.getBdId());
		}
	}

	private static void testPostsql() {
		SQLHelper sqlHelper = new SQLHelper("postsql.cfg.xml");

		List<FlightArea> models = sqlHelper.queryByField("FlightArea", "id", "066ea150927542ffb85b6be724117e7b");
		if (models.size() > 0) {
			FlightArea model = models.get(0);
			System.out.println(Spatial.writeWKT(model.getCoord()));
		}
	}
}
